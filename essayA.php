<?php 

function hitung($string)
{
    if(strpos($string,'*'))
    {
        $a = substr($string,0, strpos($string,'*'));
        $b = substr($string,strpos($string,'*')+1,strlen($string));
        $hasil = intval($a)*intval($b);
    }
    else if (strpos($string,':'))
    {
        $a = substr($string,0, strpos($string,':'));
        $b = substr($string,strpos($string,':')+1,strlen($string));
        $hasil = intval($a)/intval($b);
    }
    else if (strpos($string,'-'))
    {
        $a = substr($string,0, strpos($string,'-'));
        $b = substr($string,strpos($string,'-')+1,strlen($string));
        $hasil = intval($a)-intval($b);
    }
    else if (strpos($string,'+'))
    {
        $a = substr($string,0, strpos($string,'+'));
        $b = substr($string,strpos($string,'+')+1,strlen($string));
        $hasil = intval($a)+intval($b);
    }
    else if (strpos($string,'%'))
    {
        $a = substr($string,0, strpos($string,'%'));
        $b = substr($string,strpos($string,'%')+1,strlen($string));
        $hasil = intval($a)%intval($b);
    }
    else
    {
        $hasil = 0;
    }

    return($hasil);
}

echo "<br>";
echo '102*50 = '.hitung('102*50');
echo "<br>";
echo '200:5 = '.hitung('200:5');
echo "<br>";
echo '20+43= '.hitung('20+43');
echo "<br>";
echo '234-132 ='.hitung('234-132');
echo "<br>";
echo '15%4 ='.hitung('15%4');

?>