-- Insert rows into table 'CUSTOMERS'
INSERT INTO CUSTOMERS
( 
    Name,Email,password
)
VALUES
('John Doe','John@doe.com','John123'),
('Jane Doe','Jane@Doe.com','Jenita123');


-- Insert rows into table 'ORDERS'
INSERT INTO ORDERS
( 
    amount,customer_id
)
VALUES
(500,1),
(200,2),
(750,2),
(250,1),
(400,2);